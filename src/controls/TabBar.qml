import QtQuick 2.14
import QtQuick.Controls 2.14
import QtQuick.Layouts 1.3
import QtQuick.Templates 2.15 as T

import org.kde.kirigami 2.9 as Kirigami
import org.mauikit.controls 1.2 as Maui

import "private" as Private

/**
 * TabBar
 * A global sidebar for the application window that can be collapsed.
 *
 *
 *
 *
 *
 *
 */
T.TabBar
{
    id: control
    
    implicitWidth: _content.contentWidth
    implicitHeight: Maui.Style.rowHeight + Maui.Style.space.tiny
    //Kirigami.Theme.colorSet: Kirigami.Theme.View
    //Kirigami.Theme.inherit: false
    
    
    /**
     * showNewTabButton : bool
     */
    property bool showNewTabButton : true
    
    /**
     * newTabClicked :
     */
    signal newTabClicked()
    
    background: Rectangle
    {
        color: Kirigami.Theme.backgroundColor
    }
    
    contentItem: Item
    {        
        readonly property bool fits : _content.contentWidth <= width
        
        Loader
        {
            z: 999
            
            asynchronous: true
            width: Maui.Style.iconSizes.medium
            height: parent.height
            active: !_content.atXEnd && !parent.fits 
            visible: active
            
            anchors
            {
                right: parent.right
                top: parent.top
                bottom: parent.bottom
            }
            
            sourceComponent: Private.EdgeShadow
            {                    
                opacity: 0.7
                edge: Qt.RightEdge                    
            }
        }
        
        Loader
        {
            z: 999
            
            asynchronous: true
            width: Maui.Style.iconSizes.medium
            height: parent.height
            active: !_content.atXBeginning && !parent.fits 
            visible: active
            anchors
            {
                left: parent.left
                top: parent.top
                bottom: parent.bottom
            }
            
            sourceComponent: Private.EdgeShadow
            {
                
                opacity: 0.7
                edge: Qt.LeftEdge            
            }
        }
        
        RowLayout
        {
            anchors.fill: parent
            spacing: 0 
            
            ScrollView
            {
                id: _scrollView
                Layout.fillWidth: true
                Layout.fillHeight: true
                Layout.margins: Maui.Style.space.tiny
                                
                ScrollBar.horizontal.policy: ScrollBar.AlwaysOff
                ScrollBar.vertical.policy: ScrollBar.AlwaysOff
                
                                ListView
                {
                    id: _content
                    orientation: ListView.Horizontal
                    width: parent.width
                    height: parent.height
                    spacing: Maui.Style.space.tiny
                    model: control.contentModel
                    interactive: Maui.Handy.isTouch
                    currentIndex: control.currentIndex
                    snapMode: ListView.SnapOneItem
                    
                
                }
                
            }
            
            Loader
            {
                active: control.showNewTabButton
                visible: active
                asynchronous: true
                Layout.fillHeight: true
                Layout.preferredWidth: visible ? height : 0
                
                sourceComponent: MouseArea
                {
                    hoverEnabled: true
                    onClicked: control.newTabClicked()              
                    
                    Maui.PlusSign
                    {
                        height: Maui.Style.iconSizes.tiny
                        width: height
                        anchors.centerIn: parent
                        color: parent.containsMouse || parent.containsPress ? Kirigami.Theme.highlightColor : Qt.tint(Kirigami.Theme.textColor, Qt.rgba(Kirigami.Theme.backgroundColor.r, Kirigami.Theme.backgroundColor.g, Kirigami.Theme.backgroundColor.b, 0.7))
                    }
                }
            }
        }
    }
}
